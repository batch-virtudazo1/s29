// db.products.insertMany([
// 
//     {
//             "name": "Iphone X",
//             "price": 30000,
//             "isActive": true
//     },
//      {
//             "name": "Samsung Galaxy s21",
//             "price": 51000,
//             "isActive": true
//     },
//      {
//             "name": "Razer Blackshark V2X",
//             "price": 2800,
//             "isActive": false
//     },
//      {
//             "name": "RAKK Gaming Mouse",
//             "price": 1800,
//             "isActive": true
//     },
//      {
//             "name": "Razer Mechanical Keyboard",
//             "price": 4000,
//             "isActive": true
//     }
// 
// ])

// Query Opeators
// allows us to expand our queries and define conditions instead if just looking for a fixed values.
// to run a specific code block, highlight it then press run
// $gt, %gte, $lt, $lte
// db.products.find({price:{$gt:3000}})
// db.products.find({price:{$lt:3000}})
//db.products.find({price:{$gte:30000}})
//db.products.find({price:{$lte:2800}})

// db.users.insertMany([
//     
//     {
//         firstname: "Mary Jane",
//         lastnmae: "Watson",
//         email: "mjtiger@gmail.com",
//         password: "tigerjackpot15",
//         isAdmin: false
//         
//     },
//     {
//         firstname: "Gwen",
//         lastnmae: "Stacy",
//         email: "stacyTech@gmail.com",
//         password: "stacyTech1991",
//         isAdmin: true
//         
//     },
//     {
//         firstname: "Peter",
//         lastnmae: "Parker",
//         email: "peterWebDev@gmail.com",
//         password: "webdeveloperPeter",
//         isAdmin: true
//         
//     },
//     {
//         firstname: "Jonah",
//         lastnmae: "Jameson",
//         email: "jjjameson@gmail.com",
//         password: "spideyisamenance",
//         isAdmin: false
//         
//     },
//     {
//         firstname: "Otto",
//         lastnmae: "Octavius",
//         email: "ottoOctopi@gmail.com",
//         password: "doc0ck15",
//         isAdmin: false
//         
//     }
//     
// ])

// $regex - allows us to find documents which will match the
// characters/pattern of the characters we are looking for.
//  case sensitive. it should the same if capital and capital only.
// by default case sensitive
// db.users.find({firstname: {$regex: 'O'}})

// $options - used our regex will be case insensitive
// db.users.find({firstname: {$regex: 'o', $options: '$i'}})
//  we can also find for documents with partial matches
// db.products.find({name: {$regex: 'phone', $options: '$i'}})

// find users whoe email have the word "web" in it.
// db.users.find({email: {$regex: 'web', $options: '$i'}})

// db.products.find({name: {$regex: 'razer', $options: '$i'}})
// db.products.find({name: {$regex: 'rakk', $options: '$i'}})


// $or 
// db.products.find({$or:[{name: {$regex: 'x', $options: '$i'}},{price: {$lte:10000}}]})
// db.products.find({$or:[{name: {$regex: 'x', $options: '$i'}},{price: {$gte:30000}}]})
// // $and 
// db.products.find({$and:[{name: {$regex: 'razer', $options: '$i'}},{price: {$gte:3000}}]})
// db.products.find({$and:[{name: {$regex: 'x', $options: '$i'}},{price: {$gte:30000}}]})
// db.products.find({$and:[{name:{:,:}},{:{}}]})
// db.users.find({$and:[{lastnmae: {$regex: 'w', $options: '$i'}},{isAdmin: false}]})
// db.users.find({$and:[{firstname: {$regex: 'a', $options: '$i'}},{isAdmin: true}]})

// Field projection - allows us to show(1)/hide(0) certain properties
db.users.find({},{_id:0,password:0})
db.users.find({isAdmin: true},{_id:0,email:1})
db.users.find({isAdmin: false},{firstname:1,lastnmae:1})

// hide the id
db.products.find({price: {$gte:10000}},{_id:0,name:1,price:1})